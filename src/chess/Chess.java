/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author Mhmd Shams Aldeen
 */
public class Chess {

    public static void main(String[] args) {   

        Scanner input = new Scanner(System.in);
        System.out.println("enter 1 for DFS \t 2 for BFS \t 3 for UCS \t 4 for A_STAR \t 5 for exit");
        int x = input.nextInt();

        Structure structure = new Structure(0, 1, 8, 7);


        Logic logic = new Logic(structure);

        
        int counter =0 ;
        switch (x) {
            case 1: 
            {
                
                long startTime = System.nanoTime();
                logic.DFS(structure);
                long endTime = System.nanoTime();
                
                Structure s = Logic.DFS_RESULT;

                while (s != null) {
                    counter++;
                    s.grid();
                    System.out.println("------------------");
                    s = s.parent;
                }
                System.out.println("VISITED STATES : " + Logic.DFS_VISITED_LENGHT);
                System.out.println("size of path : " + counter);
                System.out.println("time in milli : " + (float)(endTime-startTime)/1000000);
                break;
            }

            case 2: 
            {
                
                long startTime = System.nanoTime();
                
                logic.BFS(structure);
                
                long endTime = System.nanoTime();
                
                
                Structure s = Logic.BFS_RESULT;
                while (s != null) {
                    counter++;
                    s.grid();
                    System.out.println("------------------");
                    s = s.parent;
                }
                System.out.println("VISITED STATES : " + Logic.BFS_VISITED_LENGHT);
                System.out.println("size of path : " + counter);
                System.out.println("time in milli : " + (float)(endTime-startTime)/1000000);
                break;
            }

            case 3: 
            {
                
                long startTime = System.nanoTime();
                logic.UCS(structure);
                long endTime = System.nanoTime();
                
                Structure s = Logic.UCS_RESULT;
                while (s != null) {
                    counter++;
                    s.grid();
                    System.out.println("------------------");
                    s = s.parent;
                }
                System.out.println("VISITED STATES : " + Logic.UCS_VISITED_LENGHT);
                System.out.println("size of path : " + counter);
                System.out.println("time in milli : " + (float)(endTime-startTime)/1000000);
                break;
            }
            
            case 4:
                {
                
                    long startTime = System.nanoTime();
                    logic.A_STAR(structure);
                    long endTime = System.nanoTime();

                    Structure s = Logic.A_STAR_RESULT;
                    while (s != null) {
                        counter++;
                        s.grid();
                        System.out.println("------------------");
                        s = s.parent;
                    }
                    System.out.println("VISITED STATES : " + Logic.A_STAR_VISITED_LENGHT);
                    System.out.println("size of path : " + counter);
                    System.out.println("time in milli : " + (float)(endTime-startTime)/1000000);
                    break;
            }

            case 5: {
                return;
            }

            default:
                System.out.println("wrong input");
        }

    }

}
