package chess;

import static chess.Structure.board;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Stack;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class Logic {

    Structure board;
//    static int counter =0 ;
    public ArrayList<Structure> solutions = new ArrayList<>();

    static int counter = 0;
    static Structure BFS_RESULT = new Structure(0, 0, 0, 0);
    static Structure DFS_RESULT = new Structure(0, 0, 0, 0);
    static Structure UCS_RESULT = new Structure(0, 0, 0, 0);
    static Structure A_STAR_RESULT = new Structure(0,0,0,0);
    
    static int BFS_VISITED_LENGHT ;
    static int DFS_VISITED_LENGHT ;
    static int UCS_VISITED_LENGHT ;
    static int A_STAR_VISITED_LENGHT;
    
    ArrayList<Structure> visited = new ArrayList<>();

    public Logic(Structure board) {
        this.board = board;

    }

    public void user_play() {

        Structure newstate = this.board;

        //--------input:
        System.out.println("rock move :");

        int step;
        while (!newstate.isFinal()) {

            newstate = new Structure(newstate.rock.x, newstate.rock.y, newstate.xking, newstate.yking);
            step = new Scanner(System.in).nextInt();

            while (step + newstate.rock.y >= 15) {
                //array index out of bounds
                System.out.print("Illigale input...rock move : ");
                step = new Scanner(System.in).nextInt();
            }

            if (Structure.board[newstate.rock.x][newstate.rock.y + step] == 0) {
                newstate.rock.y += step;
            }

            while (newstate.rock.x <= 7 && Structure.board[newstate.rock.x + 1][newstate.rock.y] == 0) {
                if (Structure.board[newstate.rock.x + 1][newstate.rock.y] == 0) {
                    newstate.rock.x++;
                }
            }

            newstate.grid();

            if (!newstate.isFinal()) {

                LinkedList<Rock> aviable_moves = newstate.check_moves();
                System.out.print("AVIABLE MOVES : ");
                for (Rock s : aviable_moves) {
                    System.out.print("[" + s.x + "," + s.y + "]\t");
                }
                System.out.println();
            }
        }
    }

    
    public boolean _isVisited(Structure str) {
        for (Structure s : visited) {
            if (str.isEqual(s)) 
            {
                return true;
            }
        }
        return false;
    }


    public boolean rIsParent(Structure pop, Structure next_state) {
        Structure s = pop.parent;

        while (s != null) {

            if (next_state.isEqual(s)) {
                return true;
            }
            s = s.parent;
        }
        return false;
    }


    public boolean rVisited(Structure next_state) {

        for (Structure s : visited) {
            if (Structure.rEqual(s, next_state)) {
                return true;
            }
        }
        return false;
    }

    public void DFS(Structure structure) {
        
        Stack<Structure> stack = new Stack<>();
        stack.push(structure);

        while (!stack.empty()) {
                counter++;
            Structure s = stack.pop();
            if (s.isFinal()) {
                System.out.println("King is dead");

                DFS_RESULT = s;
                DFS_VISITED_LENGHT = visited.size();
                break;
            }

            LinkedList<Structure> next_states = s.getNextState();

            for (Structure str : next_states) {
                str.parent = s;
                if (!rIsParent(s, str) || !rVisited(str)) {
                    visited.add(str);
                    stack.push(str);
                }
            }
        }

    }

  
    public void BFS(Structure structure) {
        int counter = 0;
        Queue<Structure> queue = new LinkedList<>();

        queue.add(structure);

        while (!queue.isEmpty()) {

            Structure s = queue.poll();
            if (s.isFinal()) {
                System.out.println("King is dead");
                counter++;
                if (counter == 1) {
                    BFS_RESULT = s;
                    BFS_VISITED_LENGHT = visited.size();

                    break;
                }
                continue;

            }

            LinkedList<Structure> next_states = s.getNextState();

            for (Structure str : next_states) {
                str.parent = s;
                if (!rIsParent(s, str) || !rVisited(str)) {
                    visited.add(str);
                    queue.add(str);
                }
            }
        }

    }
    
    
    public void UCS(Structure structure) {

        PriorityQueue<Structure> queue = new PriorityQueue<>();

        queue.add(structure);

        while (!queue.isEmpty()) {

            Structure s = queue.poll();
            if (s.isFinal()) {
                System.out.println("King is dead");
                UCS_RESULT = s;
                UCS_VISITED_LENGHT = visited.size();

                break;
            }

            LinkedList<Structure> next_states = s.getNextState();

            for (Structure str : next_states) {
                str.parent = s;
//                str.heurstic = str.cost ;
                if (!rIsParent(s, str) || !rVisited(str)) {

                   
                    visited.add(str);
                    queue.add(str);
                }
                }
            }
        }
    
    
    public void A_STAR(Structure structure) {

        PriorityQueue<Structure> queue = new PriorityQueue<>();

        queue.add(structure);

        while (!queue.isEmpty()) {

            Structure s = queue.poll();
            if (s.isFinal()) {
                System.out.println("King is dead");
                A_STAR_RESULT = s;
                A_STAR_VISITED_LENGHT = visited.size();

                break;
            }

            LinkedList<Structure> next_states = s.getNextState();

            for (Structure str : next_states) {
                str.parent = s;
                 
                str.h = str.cost+str.getStateHeuristics();
     
                if (!rIsParent(s, str) || !rVisited(str)) {
                   
                    visited.add(str);
                    queue.add(str);
                }
                }
            }
        }
    

    }

