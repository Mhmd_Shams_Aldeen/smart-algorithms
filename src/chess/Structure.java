package chess;

import chess.Logic;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;
import java.util.Stack;

class Structure implements Cloneable, Comparable<Structure> {

    int xpos, ypos, x, y, xking, yking;

    int cost;

    int h ;
    
    public Structure parent;

    Rock rock = new Rock(0, 0);

    static int starting_heuristic = 10;

    static int heuristic = 100;

    public static Scanner input = new Scanner(System.in);
    public static int[][] board = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1},
        {0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0},};

    public Structure(int x, int y, int xking, int yking) {
        this.x = rock.x;
        this.y = rock.y;
        this.xking = xking;
        this.yking = yking;

        this.cost = 100000;
    }

    public void grid() {

        for (int i = 0; i < board.length; i++) {
            System.out.printf("%3s%3s", "\033[1;30m", " | ");
            for (int j = 0; j < board[i].length; j++) {
                if (i == this.rock.x && j == this.rock.y) {
                    System.out.printf("%3s%3s", "\033[0;31m", "\u2656");
                } else if (i == this.xking && j == this.yking) {
                    if (this.isFinal()) {
                        System.out.printf("%3s%3s", "\033[0;34m", "\u2656");
                    } else {
                        System.out.printf("%3s%3s", "\033[0;34m", "\u2654");
                    }
                } else {
                    if (board[i][j] == 0) {
                        System.out.printf("%3s%3d", "\033[0;37m", board[i][j]);
                    }
                    if (board[i][j] == 1) {
                        System.out.printf("%3s%3d", "\033[0;32m", board[i][j]);
                    }
                }
            }
            System.out.printf("%3s%3s", "\033[1;30m", "|");
            System.out.println();
        }
    }

    public boolean isFinal() {

        return this.xking == this.rock.x && this.yking == this.rock.y;
    }

    public boolean isEqual(Structure str) {

        boolean positions_check = (this.rock.x == str.rock.x) && (this.rock.y == str.rock.y);

        return positions_check;
    }

    public static boolean rEqual(Structure s1, Structure s2) {
        if ((s1 == null && s2 != null) || (s2 == null && s1 != null)) {
            return false;
        }

        if (s1.rock.x != s2.rock.x && s1.rock.y != s2.rock.y) {
            return false;
        }

        if (s1.parent == null && s2.parent == null) {
            return true;
        }

        return rEqual(s1.parent, s2.parent);
    }

    public LinkedList check_moves() {
        LinkedList<Rock> aviable_moves = new LinkedList<>();

        int castle_y = this.rock.y;
        while (castle_y <= 13) {
            if (Structure.board[this.rock.x][castle_y + 1] == 0) {
                int xx = this.rock.x;
                while (xx < 8 && Structure.board[xx + 1][castle_y + 1] == 0) {
                    xx++;
                }
                aviable_moves.add(new Rock(xx, castle_y + 1));
                castle_y++;
            }
        }
        castle_y = this.rock.y;
        while (castle_y >= 1) {

            if (Structure.board[this.rock.x][castle_y - 1] == 0) {
                int xx = this.rock.x;
                while (xx < 8 && Structure.board[xx + 1][castle_y - 1] == 0) {
                    xx++;
                }
                aviable_moves.add(new Rock(xx, castle_y - 1));
                castle_y--;
            }
        }

        return aviable_moves;
    }

    public Structure deep_copy() {

        Structure newStructure = new Structure(this.rock.x, this.rock.y, xking, yking);

        return newStructure;
    }

    public LinkedList<Structure> getNextState() {

        LinkedList<Structure> nextStates = new LinkedList<>();
        LinkedList<Rock> possible_positions = this.check_moves();

        for (Rock rock : possible_positions) {
            Structure s = deep_copy();
            s.rock.x = rock.x;
            s.rock.y = rock.y;
            s.x = rock.x;
            s.y = rock.y;
            s.cost = cost + 1;

            s.h = cost+1;
            nextStates.add(s);
        }

        return nextStates;
    }

    public int getStateHeuristics() {
        boolean line_has_wall = false;

        if (this.rock.x == this.xking) {
            //that means that this state is a final state
            if (this.rock.y == this.yking) {
                return 0;
            } else {
                if (this.yking > this.rock.y) {
                    int yy = this.rock.y + 1;
                    while (yy != this.yking) {
                        if (Structure.board[this.rock.x][yy] == 1) {
                            line_has_wall = true;
                            break;
                        } else {
                            yy++;
                        }
                    }

                } else {
                    int yy = this.rock.y - 1;
                    while (yy != this.yking) {
                        if (Structure.board[this.rock.x][yy] == 1) {
                            line_has_wall = true;
                            break;
                        } else {
                            yy--;
                        }
                    }
                }
            }

            if (!line_has_wall) {
                return 1;
            }
        }

        LinkedList<Structure> next_states = this.getNextState();
        if (next_states.size() == 0) {
            return Integer.MAX_VALUE;
        }
        if (this.rock.x > this.parent.rock.x) {
            return Structure.starting_heuristic++;
        } else {
            return Structure.heuristic++;
        }

    }

    @Override
    public int compareTo(Structure t) {
        if ( h> t.h) {
            return 1;
        } else if (h < t.h) {
            return -1;
        }
        return 0;
    }

}
