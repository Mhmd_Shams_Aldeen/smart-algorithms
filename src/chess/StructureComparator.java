/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess;

import java.util.Comparator;

/**
 *
 * @author Mhmd Shams Aldeen
 */
public class StructureComparator implements Comparator<Structure>{

    @Override
    public int compare(Structure t, Structure t1) {
        
        if(t.cost<t1.cost)
            return 1 ;
        else if(t.cost>t1.cost)
            return -1 ;
        return 0 ;
    }
    
    
    
}
